from pydantic import BaseModel
import redis
import csv
from typing import Dict, Optional, Union, List
from fastapi import FastAPI
from redis.exceptions import ResponseError
from redisearch import Client, TextField, NumericField, Query
from fastapi.middleware.cors import CORSMiddleware
import matplotlib.pyplot as plt

r = redis.StrictRedis(host='localhost', port=6381, db=0)

# Lire le fichier CSV
with open('data/logements-et-logements-sociaux-dans-les-departements.csv', mode='r', encoding='utf-8-sig') as file:
    csv_reader = csv.DictReader(file, delimiter=';')
    next(csv_reader)  
    for row in csv_reader:
        nombre_d_habitants = row['nombre_d_habitants']  
        r.hset(nombre_d_habitants, mapping=row)  # Stocker les données dans Redis 

# Créer l'index Redisearch
client = Client('idx:Logement', conn=r)
try:
    # Création de l'index
    client.create_index([
        TextField('annee_publication'),
        TextField('code_departement'),
        TextField('nom_departement'),
        TextField('code_region'),
        TextField('nom_region'),
        TextField('nombre_d_habitants'),
        TextField('densite_de_population_au_kmdeux'),
        TextField('variation_de_la_population_sur_dix_ans_en'),
        TextField('dont_contribution_du_solde_naturel_en'),
        TextField('dont_contribution_du_solde_migratoire_en'),
        TextField('population_de_moins_de_vingt_ans'),
        TextField('population_de_soixante_ans_et_plus'),
        TextField('taux_de_chomage_au_tquatre_en'),
        TextField('taux_de_pauvrete_en'),
        TextField('nombre_de_logements'),
        TextField('nombre_de_residences_principales'),
        TextField('taux_de_logements_sociaux_en'),
        TextField('taux_de_logements_vacants_en'),
        TextField('taux_de_logements_individuels_en'),
        TextField('moyenne_annuelle_de_la_construction_neuve_sur_dix_ans'),
        TextField('construction'),
        TextField('parc_social_nombre_de_logements'),
        TextField('parc_social_logements_mis_en_location'),
        TextField('parc_social_logements_demolis'),
        TextField('parc_social_ventes_a_des_personnes_physiques'),
        TextField('parc_social_taux_de_logements_vacants_en'),
        TextField('parc_social_taux_de_logements_individuels_en'),
        TextField('parc_social_loyer_moyen_en_eur_mdeux_mois'),
        TextField('parc_social_age_moyen_du_parc_en_annees'),
        TextField('parc_social_taux_de_logements_energivores_e_f_g_en'),
    ])
except ResponseError:
    pass  # L'index existe déjà, pas besoin de le recréer

# Ajouter les données au Redisearch index
def add_data_to_redisearch():
    keys = r.keys('*')
    for key in keys:
        data = r.hgetall(key)
        try:
            # Convertir les clés en chaînes de caractères
            string_keys_data = {k.decode('utf-8').replace(' ', '_'): v.decode('utf-8') for k, v in data.items()}
            
            # Ajouter le document dans l'index Redisearch avec l'option REPLACE
            client.add_document(key.decode('utf-8'), replace=True, **string_keys_data)
        except ResponseError as e:
            print(f"Erreur lors de l'ajout du document {key.decode('utf-8')}: {e}")


# Fonction de recherche dans l'index Redisearch
def search_index(query_string):
    results = client.search(Query(query_string).verbatim())
    return results

# Exemple d'utilisation de la fonction de recherche
results = search_index('681361')
print(results.docs)



class SearchParams(BaseModel):
  nom_departement: Optional[str] = None
  annee_publication: Optional[str] = None
  nom_region: Optional[str] = None
  nombre_de_logements: Optional[str] = None




# Initialisation de l'application FastAPI
app = FastAPI()


# Configuration CORS pour autoriser les requêtes depuis localhost:4200 
origins = [
    "http://localhost:4200", 
    "http://localhost:8000",  # Ajoutez ici l'origine de votre application front-end
 
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)




# Endpoint racine pour récupérer les clés depuis Redis
@app.get("/")
def read_root():
    nombre_d_habitants = []

    # Récupérer toutes les clés dans Redis
    keys = r.keys('*') 

    # Afficher les données associées à chaque clé
    for key in keys:
        try:
            nombre_d_habitants.append(key.decode('utf-8'))
        except Exception as e:
            print(f"Erreur: {e}")
    return {"nombre_d_habitants": nombre_d_habitants}

@app.get("/data", response_model=List[Dict[str, str]])
async def get_data():
    keys = r.keys('*')
    data = [r.hgetall(key) for key in keys]
    return data

# Endpoints pour récupérer des données selon différents critères de recherche
@app.get("/codedepartement/{code_departement}")
def get_codedep(code_departement: str, q: Union[str, None] = None):
    results = client.search(f"@code_departement:{code_departement}")
    return {"code_departement": results.docs, "q": q}

@app.get("/nomdepartement/{nom_departement}")
def get_nomdep(nom_departement: str, q: Union[str, None] = None):
    results = client.search(f"@nom_departement:{nom_departement}")
    return {"nom_departement": results.docs, "q": q}

@app.get("/anneepublication/{annee_publication}")
def get_nomdep(annee_publication: str, q: Union[str, None] = None):
    results = client.search(f"@annee_publication:{annee_publication}")
    return {"annee_publication": results.docs, "q": q}

@app.get("/nomregion/{nom_region}")
def get_nomreg(nom_region: str, q: Union[str, None] = None):
    results = client.search(f"@nom_region:{nom_region}")
    return {"nom_region": results.docs, "q": q}

@app.get("/nomregionall")
def getallreg():
    try:
        results = client.search("@nom_region:*")
        return {"nom_region": results.docs}
    except Exception as e:
        return {"error": str(e)}


@app.get("/habitantall")
def get_haball():
    try:
        results = client.search("@nombre_d_habitants:*")
        return {"nombre_d_habitants": results.docs}
    except Exception as e:
        return {"error": str(e)}

@app.get("/nombrehabitant/{nombre_d_habitants}")
def gethab(nombre_d_habitants: str, q: Union[str, None] = None):
    results = client.search(f"@nombre_d_habitants:{nombre_d_habitants}")
    return {"nombre_d_habitants": results.docs, "q": q}

@app.get("/nombredelogements/{nombre_de_logements}")
def get_nblog(nombre_de_logements: str, q: Union[str, None] = None):
    results = client.search(f"@nombre_de_logements:{nombre_de_logements}")
    return {"nombre_de_logements": results.docs, "q": q}

@app.get("/chomage/{taux_de_chomage_au_tquatre_en}")
def get_chomage(taux_de_chomage_au_tquatre_en: str, q: Union[str, None] = None):
    results = client.search(f"@taux_de_chomage_au_tquatre_en:{taux_de_chomage_au_tquatre_en}")
    return {"taux_de_chomage_au_tquatre_en": results.docs, "q": q}

@app.get("/search/{query_string}")
def get_services(query_string: str):
    results = client.search(f"{query_string}*")
    return results.docs


@app.get("/recherche")
async def search(nom_departement: Optional[str] = None,taux_de_chomage_au_tquatre_en: Optional[int] = None, nombre_d_habitants: Optional[int] = None, annee_publication: Optional[int] = None, nom_region: Optional[str] = None, nombre_de_logements: Optional[int] = None):
    if not any([nom_departement,taux_de_chomage_au_tquatre_en, annee_publication, nombre_d_habitants,nom_region, nombre_de_logements]):
        query = Query("*").paging(0, 1000)

        results = client.search(query)
    else:
        query_parts = []
        if nom_departement:
            query_parts.append(f"@nom_departement:{nom_departement}")
        if annee_publication:
            query_parts.append(f"@annee_publication:{annee_publication}")
        if nom_region:
            query_parts.append(f"@nom_region:{nom_region}")
        if nombre_d_habitants:
            query_parts.append(f"@nombre_d_habitants:{nombre_d_habitants}")
        if nombre_de_logements:
            query_parts.append(f"@nombre_de_logements:{nombre_de_logements}")
        if taux_de_chomage_au_tquatre_en:
            query_parts.append(f"@taux_de_chomage_au_tquatre_en:{taux_de_chomage_au_tquatre_en}")

        query_string = " ".join(query_parts)
        query = Query(query_string).paging(0, 10000)  # Limite les résultats à 10000
        results = client.search(query)
    
    return results.docs

