const redis = require('redis');
const fs = require('fs');
const csv = require('csv-parser');

async function importData(filePath) {
  const client = redis.createClient(); // Création d'une connexion à Redis

  client.on('error', (err) => {
    console.error('Erreur lors de la connexion à Redis :', err);
  });

  const processData = async (row) => {
    const key = row['nombre_d_habitants'];
    const data = Object.entries(row).flat(); // Convertir les données en un tableau alternant clés et valeurs

    return new Promise((resolve, reject) => {
      const args = [key, ...data]; // Arguments pour HSET (clé, champ1, valeur1, champ2, valeur2, ...)
      client.send_command('HSET', args, (err, res) => {
        if (err) {
          reject(err);
        } else {
          console.log(`Données pour ${key} insérées dans Redis.`);
          resolve(res);
        }
      });
    });
  };

  const dataPromises = [];

  const readStream = fs.createReadStream(filePath)
    .pipe(csv({ separator: ';' }));

  for await (const row of readStream) {
    const dataPromise = processData(row);
    dataPromises.push(dataPromise);
  }

  try {
    await Promise.all(dataPromises);
    console.log('Import des données terminé.');
  } catch (err) {
    console.error(err);
  } finally {
    client.quit(); // Fermeture de la connexion à Redis après l'import
  }
}

importData('../data/logements-et-logements-sociaux-dans-les-departements.csv');