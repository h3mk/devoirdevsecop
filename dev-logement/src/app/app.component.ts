import { Component, OnInit } from '@angular/core';
import { LogementService } from './logement.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'chart.js';
import { Chart, BarController, LinearScale, CategoryScale, ChartItem, BarElement } from 'chart.js';
import { forkJoin } from 'rxjs';

Chart.register(BarController, BarElement, CategoryScale, LinearScale);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit{
  constructor(private logementService: LogementService, private http: HttpClient) { }
  showTable = false;

  query_string = '';
  results: Result[] = [];
  resultsse: Result[] = [];
  selectedOptions = ['default']; 
  showDetails = false;

  nomdepartement: string | undefined;
  nomregion: string | undefined;
  nombredelogements: string | undefined;
  anneepublication: string | undefined;

  data: any[] = [];

  filteredData: any[] = [];

  populationData: any[] | undefined;
  
  ngOnInit() {
    this.http.get<any[]>('http://localhost:8000/recherche').subscribe(data => {
      this.data = data;
    });
  
    this.logementService.get_all().subscribe(data => {
      this.allData = Object.keys(data).map(key => ({key: key, value: data[key]}));
    });

    /* this.logementService.getservices(this.query_string).subscribe(data => {
      this.populationData = data;
          }); */
          this.http.get<any[]>('http://localhost:8000/data').subscribe(data => {
            this.allData = data;
            console.log("yoooo",data);
          });

    this.tableau()
    this.tableauDeux()
    this.tableauTrois()
  }

  allData: any[] = [];


  tableau() {
    this.http.get<any[]>('http://localhost:8000/data').subscribe(data => {
      this.allData = data;
      const nomDeRegion = this.allData.map(item => item.nom_region);
      const nombreHabitant = this.allData.map(item => item.nombre_d_habitants);
  
      console.log("yo",nomDeRegion, nombreHabitant);

      const ctx = document.getElementById('myChart') as ChartItem;
      new Chart(ctx, {
        type: 'bar',
        data: {
          labels: nomDeRegion,
          datasets: [{
            label: 'Nombre d\'habitants par région',
            data: nombreHabitant,
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
    } );
    }
        

    tableauDeux() {
      this.http.get<any[]>('http://localhost:8000/data').subscribe(data => {
        this.allData = data;
        const nomDeRegion = this.allData.map(item => item.nom_region);
        const tauxchomage = this.allData.map(item => item.taux_de_chomage_au_tquatre_en);
    
        console.log("yo",nomDeRegion, tauxchomage);
  
        const ctx = document.getElementById('myChart2') as ChartItem;
        new Chart(ctx, {
          type: 'bar',
          data: {
            labels: nomDeRegion,
            datasets: [{
              label: 'Taux de chomage par région',
              data: tauxchomage,
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          }
        });
      } );
      }
    

      tableauTrois() {
        this.http.get<any[]>('http://localhost:8000/data').subscribe(data => {
          this.allData = data;
          const nomDeRegion = this.allData.map(item => item.nom_region);
          const nbrconstruction = this.allData.map(item => item.construction);
      
          console.log("yo",nomDeRegion, nbrconstruction);
    
          const ctx = document.getElementById('myChart3') as ChartItem;
          new Chart(ctx, {
            type: 'bar',
            data: {
              labels: nomDeRegion,
              datasets: [{
                label: 'Taux de chomage par région',
                data: nbrconstruction,
                borderWidth: 1
              }]
            },
            options: {
              scales: {
                y: {
                  beginAtZero: true
                }
              }
            }
          });
        } );
        }
      
  
 

  get uniqueDepartments() {
    const departments = this.data.map(item => item.nom_departement);
    return [...new Set(departments)];
  }

  get uniqueRegions() {
    const regions = this.data.map(item => item.nom_region);
    return [...new Set(regions)];
  }
  
  get uniqueDepartmentCodes() {
    const codes = this.data.map(item => item.code_departement);
    return [...new Set(codes)];
  }
  
  filters = {
    code_departement: '',
    nombre_de_logements: '',
    nom_departement: '',
    nom_region: '',
    nombre_d_habitants: '',
    taux_de_chomage_au_tquatre_en: '',
    construction: '',
  };
  

  applyFilters() {
    let params = new HttpParams();
    for (const [key, value] of Object.entries(this.filters)) {
      if (value) {
        params = params.set(key, value);
      }
    }
  
    this.http.get<any[]>('http://localhost:8000/recherche', {params: params}).subscribe(data => {
  this.filteredData = data;
});
    
  }
  

  selectedFilterType = 'exact'; // Initial value for the filter type



  



  onSearche() {
    const searchParams = {
      nom_departement: this.nomdepartement,
      annee_publication: this.anneepublication,
      nom_region: this.nomregion,
      nombre_de_logements: this.nombredelogements
    };
  
    this.logementService.search(searchParams).subscribe(data => {
      this.resultsse = data;
    });
  }
/* 
  onSearch(): void {
    this.results = []; 

    this.selectedOptions.forEach(option => {
      switch (option) {
        case 'nomdepartement':
          this.logementService.getnomdep(this.query_string).subscribe(data => {
            this.results = this.results.concat(data.nom_departement);
          });
        break;

        case 'anneepublication':
          this.logementService.getnbannee(this.query_string).subscribe(data => {
            this.results = this.results.concat(data.annee_publication);
          });
        break;

        case 'nomregion':
          this.logementService.getnomreg(this.query_string).subscribe(data => {
            this.results = this.results.concat(data.nom_region);
          });        
        break;

        case 'nombredelogements':
          this.logementService.get_nblog(this.query_string).subscribe(data => {
            this.results = this.results.concat(data.nombre_de_logements);
          });        
        break;

        case 'all':
          this.logementService.getservices(this.query_string).subscribe(data => {
            this.results = this.results.concat(data);
          });        
        break;
      
        default:
          this.logementService.getservices(this.query_string).subscribe(data => {
            this.results = this.results.concat(data);
          });
        break;
    }
  }
)} */
} 

export interface Result {
  nom_departement: string;
  code_region: string;
  nom_region: string;
  nombre_de_logements: number;
  code_departement: string;
  taux_de_pauvrete: string;
  logements_mis_en_location: string;
  taux_de_chomage_au_tquatre_en: number;

  
  nombre_d_habitants: number;
  taux_de_logements_vacants: string;
  taux_de_logements_individuels: string;
  loyer_moyen_en_euros_par_mois:  string;

}