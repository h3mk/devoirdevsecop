// logement.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// logement.service.ts
@Injectable({
  providedIn: 'root'
})
export class LogementService {
  private apiUrl = 'http://localhost:8000/'; 

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(this.apiUrl);
  }

  search(searchParams: any): Observable<any> {
    return this.http.post(`${this.apiUrl}recherche`, searchParams);
  }

  getnomdep(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}nomdedepartement/${query_string}`);
  }

  getnbannee(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}anneepublication/${query_string}`);
  }

  getcodereg(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}coderegion/${query_string}`);
  }

  getnomreg(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}nomregion/${query_string}`);
  }

  gethab(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}nombrehabitant/${query_string}`);
  }

  getallreg(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}nomregionall`);
  }

  get_nblog(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}nombredelogements/${query_string}`);
  }

  get_haball(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}habitantall`);
  }

  get_all(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}all`);
}
  
  get_chomage(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}chomage/${query_string}`);
  }

  /* getservices(query_string: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}search/${query_string}`);
  } */
}
